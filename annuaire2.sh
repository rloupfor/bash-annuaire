#!/bin/bash

menu(){
clear
cat -n contenu_annuaire.txt
nombre_entree=$(cat contenu_annuaire.txt | wc -l)
echo "Le nombre d'entrées est $nombre_entree."
echo "Que voulez-vous faire ?"
echo "1. Créer un nouvel abonné"
echo "2. Visualiser le contenu de annuaire trié par nom"
echo "3. Visualiser le contenu de annuaire trié par ville"
echo "4. Supprimer un abonné"
echo "5. Visualiser les informations sur un abonné"
echo "6. Quitter"

read choix

case $choix in
	[1]*)	clear
		 echo "Création d'un nouvel abonné"
		nouvel_abonne;;
	[2]*) 	clear
		echo "Visualisation contenu de l'annuaire trié par nom"
		visualisation_tri_nom;;
	[3]*) 	clear
		echo "Visualisation contenu de l'annuaire trié par ville"
		visualisation_tri_ville;;
	[4]*) 	clear
		echo "Suppression d'un abonné"
		suppression_abonne;;
	[5]*) 	clear
		echo "Visualisation information abonné"
		information_abonne;;
	[6]*) echo "A bientot";;
	*) menu;;
esac
}



nouvel_abonne(){
	echo "Veuillez entrer votre nom num_tel ville"
	read nom num_tel ville
	if [[ ! $nom ]]; then
		echo "Il faudrait entrer un nom."
		nouvel_abonne
		return
	fi
	if [[ ! $num_tel ]]; then
		echo "Veuillez entrer votre numéro de téléphone."
		nouvel_abonne
		return
	fi
	if [[ ! $num_tel =~ ^[0-9]{10}$ ]]; then
		echo "Veuillez entrer un numéro de téléphone à 10 chiffres."
		nouvel_abonne
		return
	fi

	if [[ ! $ville ]]; then
		echo "Il faudrait entrer une ville."
		nouvel_abonne
		return
	fi
	
	echo "${nom^^}:$num_tel:$ville" >> contenu_annuaire.txt
	echo 'Pour revenir au menu, tapez "menu"'
	read entree
	if [[ $entree == "menu" ]]; then
		menu
		return
	fi
	nouvel_abonne
}

visualisation_tri_nom(){
	echo "Contenu de l'annuaire trié par noms"
	sort contenu_annuaire.txt
}

visualisation_tri_ville(){
	echo "Contenu de l'annuaire trié par villes"
	sort -t: -k  3 contenu_annuaire.txt
}

suppression_abonne(){
	echo "Tapez le numéro de ligne de l'abonné que vous souhaitez supprimer"
	cat -n contenu_annuaire.txt
	read suppr_abo
	sed -i "$suppr_abo d" contenu_annuaire.txt
}


information_abonne(){
	echo "De quel abonné voulez-vous afficher les informations ?"
	read abo 
	grep ${abo^^} contenu_annuaire.txt
}


menu
